import { Component, OnInit } from '@angular/core';
import { UserRegistrationService } from '../services/user-registration.service';
import { Router } from '@angular/router';
import { PusherServiceService } from '../services/pusher-service.service';

@Component({
  selector: 'app-dashboard-new',
  templateUrl: './dashboard-new.component.html',
  styleUrls: ['./dashboard-new.component.css']
})
export class DashboardNewComponent implements OnInit {

  screenWidth: any;

  constructor(
    private _authServise: UserRegistrationService,
    private _router: Router,
    private _pusher: PusherServiceService
  ) {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      // set screenWidth on screen size change
      this.screenWidth = window.innerWidth;
    };
  }

  Username = '';
  //notifications = ['notifications 1', 'notifications 2', 'notifications 3'];

  message: { 'name': String, 'message': String };
  public notifications: Array<Object> = [];
  tab = '';


  logOut() {
    this._authServise.logUotService().subscribe(
      message => {
        //console.log(message.message)
        if (message.message === 'Successfully logged out') {
          this._router.navigate(['/login'])
          localStorage.removeItem('token')
        }
      }
    );
  }

  ngOnInit() {
    this._authServise.onCheck().subscribe(
      ress => {
        //console.log(ress);
        this.Username = ress.name;
        localStorage.setItem('akUIDura', ress.id);
        //console.log(localStorage.getItem('akUIDura'));
      }
    );

    this._authServise.GetRoleService().subscribe(
      ress => {
        localStorage.setItem('akuserroleura', ress[0].name);
      });

    this.tab = localStorage.getItem('akuserroleura');
    //this._pusher.setChannel('private\-MentorStudentMsg.1.43');//`Chat.${localStorage.getItem('akUIDura')}`);
    this._pusher.channel.bind('my-event', data => {
      //console.log(data);
      //this.message = data.mentorStudentMsg.message;
      //this.notifications.push(data.mentorStudentMsg.message);
    });
  }

  onProfile() {
    this._router.navigate(['/dash2/profile']);
  }

  onSetting() {
    this._router.navigate(['/dash2/setting']);
  }

}
