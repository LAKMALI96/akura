export class Project {
    constructor(
        
        public project_name: String,
        public project_manager_id: String,
        public project_coordinator1: String,
        public project_coordinator2: String,
        public start_date: String,
        public end_date: String,
        public location: String,
        public type: String,
        public description: String,
        public district: String,
        public city: String,
        public addres_line_1: String,
        public addres_line_2?: String,
        public zip? : String
         //*/

    ){}
}
