import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse,  } from '@angular/common/http';



const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest'
  })
}

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
public projectView:any;


  private _createProjectUrl = 'http://127.0.0.1:8000/api/auth/createProject';
  private _updateProjectUrl = 'http://127.0.0.1:8000/api/auth/updateProject/{id}';
  private _deleteproject = 'http://127.0.0.1:8000/api/auth/deleteproject';
  private _projectManagerUrl = 'http://127.0.0.1:8000/api/auth/projectManager';
 
  private _districtUrl = 'http://127.0.0.1:8000/api/auth/district';
 
  private _allProjectUrl = 'http://127.0.0.1:8000/api/auth/allProject';
  private _upAllProjectUrl = 'http://127.0.0.1:8000/api/auth/upAllProject';
  private _searchProjectUrl = 'http://127.0.0.1:8000/api/auth/searchProject';
 
  private _projectCoordinatorUrl = 'http://127.0.0.1:8000/api/auth/projectCoordinator';
  private _projectTypeUrl = 'http://127.0.0.1:8000/api/auth/type';
  private _enrollProjectUrl = 'http://127.0.0.1:8000/api/auth/enrollWorkshop';
  private _viewProjectUrl = 'http://127.0.0.1:8000/api/auth/listproject';
  


 
 
 
  constructor(private http: HttpClient, private httpClient:HttpClient) { }


   getOneProject(id){
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization':`Bearer ${localStorage.getItem('token')}`
      })
    }
     return this.httpClient.get<any>('http://127.0.0.1:8000/api/auth/getoneproject/'+id, httpOptions);

   } 
   updateProject(id,data){
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization':`Bearer ${localStorage.getItem('token')}`
      })
    }
    return this.httpClient.patch<any>('http://127.0.0.1:8000/api/auth/updateproject/'+id,data,httpOptions);
   }
   deleteProjects(id:any){
     return this.http.post<any>(this._deleteproject, {
       'id':id
     });
   }
  
  onCreatProject(application) {
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    }


    return this.http.post<any>(this._createProjectUrl, application, httpoptions);
   
  }
  onUpdateProject(application) {
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    }


    return this.http.post<any>(this._updateProjectUrl, application, httpoptions);
  }

  projectManagerGetService() {
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    }
    return this.http.get<any>(this._projectManagerUrl, httpoptions);
  }
 

  projectCoordinatorGetService() {
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    }
    return this.http.get<any>(this._projectCoordinatorUrl, httpoptions);
  }
 
  districtGetService() {
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    }
    return this.http.get<any>(this._districtUrl, httpoptions);
  }

 
  typeGetService() {
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    }
    return this.http.get<any>(this._projectTypeUrl, httpoptions);
  }

  

  projectSearchService(searchData) {
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    }
    return this.http.post<any>(this._searchProjectUrl, searchData, httpoptions);
  }
  

  allProjectService() {
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    }
    return this.http.get<any>(this._allProjectUrl, httpoptions);
  }
  upAllProjectService() {
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    }
    return this.http.get<any>(this._upAllProjectUrl, httpoptions);
  }
  viewProjectService() {
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    }
    return this.http.get<any>(this._viewProjectUrl, httpoptions);
  }
  

  onEnrollProject(application) {
    let httpoptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    }


    return this.http.post<any>(this._enrollProjectUrl, application, httpoptions);
  }
 
}
