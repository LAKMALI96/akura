import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ProjectService } from 'src/app/services/project.service';
import { Plist } from 'src/app/model/plist';
import { Router, ActivatedRoute } from "@angular/router"

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})

export class ProjectListComponent implements OnInit {
 
  Data = new FormData();
  projects:any;
  animal: string;
  name: string;
  isdeleted: boolean =false;

 
   @Input() plist: Plist;
    
    constructor(
    public dialog:MatDialog,
    private _projectService: ProjectService,
    private _snackBar: MatSnackBar,
    private router: Router,
   
  ) { }
  

  ngOnInit() {  
  
   


  }
  
  //set dynamic classes
  setClasses(){
    let classes= {
      plist:true,
      'is-participate':this.plist.participate
    }
    return classes;
  }

  onNavigate(){
    this.router.navigate([`dash2/projectHeared/updateproject/${this.plist.id}`]);
  }

  deleteField(id:any){
    console.log(id);
    console.log("delete the field");
    this._projectService.deleteProjects(id).subscribe((result)=>{
      console.log("successfully deleted..");
      console.log(result);
      this.isdeleted = true;
      this.getdata();
    })
  }

  getdata(){
    this._projectService.viewProjectService().subscribe(
      ress => {
        console.log(ress);
        this.plist = ress;
      });
  }

}
