import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { MatSnackBar } from '@angular/material';

import { Project } from 'src/app/model/project';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-update-project',
  templateUrl: './update-project.component.html',
  styleUrls: ['./update-project.component.css']
})
export class UpdateProjectComponent implements OnInit {
  [x: string]: any;

  myDate = new Date();
  managers: any;
  districts: any;
  coordinators:any;
  duplicate = false;
  types:any;
  id:any;
  data:any;
 
  constructor(
    private _projectService: ProjectService,
    private _snackBar: MatSnackBar,
    private _actroute:ActivatedRoute,
    private router: Router,

    
  ) { }


  editProjectModel= new Project('', null, null, null, null, null, '', '', '', null, '', null,'',null); 
 // project= new Project("","","","","","","","","","","","");
 
ngOnInit() {
   this.id=this._actroute.snapshot.params.id;
  // console.log(this.id);
  this.getData();
 

  
 
 

   //for get project corrdinators list
   this._projectService.projectCoordinatorGetService().subscribe(
    ress => {
      this.coordinators = ress;
    },
    error => {
      if (error.status == 404) {
      }
    }
  );

  //For get project managers list
  this._projectService.projectManagerGetService().subscribe(
    ress => {
      this.managers = ress;
    },
    error => {
      if (error.status == 404) {
      }
    }
  );

  //For get districts list
  this._projectService. districtGetService().subscribe(
    ress => {
      this.districts = ress;
    },
    error => {
      if (error.status == 404) {
        //console.log(error);
        //this.uploadBox = error.error.message;
      }
    }
  );

  //For get project Type list
  this._projectService. typeGetService().subscribe(
    ress => {
      this.types = ress;
    },
    error => {
      if (error.status == 404) {
      }
    }
  );
}



openSnackBar(message: string, action: string) {
  this._snackBar.open(message, action, {
    duration: 5000,
  });
}

//For prevent duplication of project coordinator
changeCoordinator(){
  if(this.editProjectModel.project_coordinator1==this.editProjectModel.project_coordinator2){
    this.duplicate = true;
  }
  else{
    this.duplicate = false;
  }
}

   getData(){
     this._projectService.getOneProject(this.id).subscribe(res=>{
     console.log(res);
     
     this.editProjectModel= res[0];
 })
 
   

   }
   updateData(){
    this._projectService.updateProject(this.id,this.editProjectModel).subscribe(res=>{
    this.router.navigate([`dash2/projectHeared/viewproject/`]);
    
    })

  }

}