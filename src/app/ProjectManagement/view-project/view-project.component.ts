import { Component, OnInit, Input } from '@angular/core';
import { Plist } from 'src/app/model/plist';
import { ProjectService } from 'src/app/services/project.service';
import { ViewProjects } from 'src/app/model/view-projects';

@Component({
  selector: 'app-view-project',
  templateUrl: './view-project.component.html',
  styleUrls: ['./view-project.component.css']
})
export class ViewProjectComponent implements OnInit {
  
  
  show = false;


  plists:Plist[];

  view_projectModel= new ViewProjects(null,null);
 

  constructor(
  private _projectService: ProjectService,
  ) { }

 

  ngOnInit() {
    this._projectService.viewProjectService().subscribe(
      ress => {
        console.log(ress);
        this.plists = ress;
        this.show = false;
      },
      error=>{
        this.show =true;
      }
      
      );
  }

  }


 